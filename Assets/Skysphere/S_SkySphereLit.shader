﻿Shader "Custom/S_SkySphereLit" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_PanSpeed ("Pan Speed", Range(-5,5)) = 1.0
		_Emission_Intensity ("Emission Intensity", Range(0,5)) = 1.0
		_StarsColor("Stars Color (RGB)", Color) = (1,1,1,1)
		_StarsMask("Star Mask", 2D) = "black" {}
		_StarsEmission("Star Brightness", Range(0,50)) = 1.0
		_StarsPanSpeed("Star Speed", Range(-5,5)) = -1.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		fixed4 _Color;
		float _PanSpeed;
		float _Emission_Intensity;

		fixed4 _StarsColor;
		sampler2D _StarsMask;
		float _StarsEmission;
		float _StarsPanSpeed;

		struct Input {
			float2 uv_MainTex;
			float2 uv_StarsMask;
		};

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			float2 panned_UVs = { IN.uv_MainTex.x + _Time[1] / 100 * _PanSpeed,IN.uv_MainTex.y };
			fixed4 c = tex2D (_MainTex, panned_UVs) * (_Color*_Emission_Intensity);

			// Add the star color based on star emission intensity and the current pixel on the star mask
			float2 star_panned_UVs = { IN.uv_StarsMask.x + _Time[1] / 100 * _StarsPanSpeed,IN.uv_StarsMask.y };
			c += ((_StarsEmission / 10)*_StarsColor)*tex2D(_StarsMask, star_panned_UVs);

			o.Emission = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
