﻿Shader "Unlit/S_SkySphereShader"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}

		_StarsColor("Stars Color (RGB)", Color) = (1,1,1,1)
		_StarsMask("Star Mask", 2D) = "black" {}
		_StarsEmission("Star Brightness", Range(0,50)) = 1.0
		_StarsPanSpeed("Star Speed", Range(-2,2)) = -1.0

		_PlanetTex("Planet Texture", 2D) = "black" {}
		_PlanetPanSpeedX("Planet Speed X", Range(-5,5)) = 1.0
		_PlanetPanSpeedY("Planet Y Movement", Range(0,10)) = 1.0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			fixed4 _Color;

			fixed4 _StarsColor;
			sampler2D _StarsMask;
			float _StarsEmission;
			float _StarsPanSpeed;

			sampler2D _PlanetTex;
			float _PlanetPanSpeedX;
			float _PlanetPanSpeedY;


			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// Base Space Ambiance
				fixed4 mainTexColor = tex2D(_MainTex, i.uv) * (_Color);

				// Stars
				// Add the star color based on star emission intensity and the current pixel on the star mask
				float2 star_panned_UVs = { i.uv.x + _Time[1] / 100 * _StarsPanSpeed,i.uv.y };
				mainTexColor += ((_StarsEmission / 10)*_StarsColor)*tex2D(_StarsMask, star_panned_UVs);

				// Planet
				float2 planet_panned_UVs = { i.uv.x + _Time[1] / 100 * _PlanetPanSpeedX,i.uv.y + (_SinTime[0]/50 * _PlanetPanSpeedY) };
				fixed4 planetColor = tex2D(_PlanetTex, planet_panned_UVs);


				// Add inverted planet alpha channel to the backdrop
				mainTexColor = mainTexColor.rgba*(1-planetColor.a);
				// Knock out planet pixels that have alpha 0
				planetColor = planetColor.rgba*planetColor.a;

				// Add it all together
				fixed4 col = mainTexColor + planetColor;

				return col;
			}
			ENDCG
		}
	}
}
