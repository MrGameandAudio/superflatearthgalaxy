﻿using UnityEngine;
using System.Collections;
using DG.Tweening;



public class GravityModifier : MonoBehaviour {

    [Tooltip("Change the gravitation pull for this object. all zeros means no change!")]
    public Vector3 gravitationalPull;

    [Tooltip("Rotate Object in sertain direction")]
    public Vector3 objRotation;

    private void Start()
    {
        gameObject.GetComponent<MeshRenderer>().enabled = false;
    }


    private void OnTriggerStay(Collider other)
    {
        if(gravitationalPull != new Vector3(0,0,0))
        {
            other.gameObject.GetComponent<ConstantForce>().force = gravitationalPull;
            transform.DORotate(-gravitationalPull, .5f).SetEase(Ease.InOutQuad);
        }
    }

    private void RotateObject()
    {

    }
}
